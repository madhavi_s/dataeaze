document.body.onload = function(){
//    alert("LOADED!");
    $('#navbar').removeClass("in");
//    document.getElementsByTagName('frame')[0].src = "http://dataeaze.io"
}

var clientHeight, scrollBottom, scrollTopServices, scrollTopBenefits, scrollTopConsultation, scrollTopContact;

if (top !== window){ top.location = window.location;}

function getHeightOfBody() {

    var h = window.innerHeight || document.documentElement.clientHeight ||document.body.clientHeight;
    var w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    $('.mainWindow').height(h).width(w);

    clientHeight = $(document).height();
//    var scrollTopServices = $(window).scrollTop() - $(".container3").offset().top + $(".container3").height() - 1360;
    scrollBottom = $(".container2").offset().bottom;
    scrollTopServices = $(".InnerContainer3").offset().top - 70 ;
    scrollTopBenefits = $(".BenefitsContainer").offset().top - 70 ;
    scrollTopConsultation = $(".ConsultationContainer").offset().top - 70;
    scrollTopContact = $(".container4").offset().top - 40;

}

var ua = navigator.userAgent,isMobileWebkit = /WebKit/.test(ua) && /Mobile/.test(ua);

//   For mobile screen

if(isMobileWebkit){

    $(window).on('load resize',function(){
        getHeightOfBody();
    });

    $('.DataEaseHeader').css("background-color", "black");

    $('.MenuItems').click(function () {
        $('.DataEaseHeader').css("background-color", "black");
//    $('.DataEaseImage').css("display","none");
        $('#navbar').removeClass("in");
        $('.DataEaseLogo').css({
//                "font-size": "30px",
            "text-align": "left",
//                "padding": "7px 0",
            "font-weight": "300"
        })
    });

    $(document).on('click',function(){
        $('#navbar').removeClass("in");
    });

    $('.Services').click(function () {
        $('.HeaderText').html('Services');
        $('.MenuItems').removeClass("active");
        $(this).addClass("active");
        $('html, body').animate({ scrollTop: scrollTopServices }, 'slow', 'swing', function (){});
    });

    $('.Benefits').click(function () {
        $('.MenuItems').removeClass("active");
        $(this).addClass("active");
        $('.HeaderText').html('Benefits');
        $('html, body').animate({ scrollTop: scrollTopBenefits }, 'slow', 'swing', function (){});
    });

    $('.Consultation').click(function () {
        $('.MenuItems').removeClass("active");
        $(this).addClass("active");
        $('html, body').animate({ scrollTop: scrollTopConsultation }, 'slow', 'swing', function (){});
        $('.HeaderText').html('Consultation');
    });

    $('.Contact').click(function () {
        $('.MenuItems').removeClass("active");
        $(this).addClass("active");
        $('html, body').animate({ scrollTop: scrollTopContact }, 'slow', 'swing', function (){});
        $('.HeaderText').html('Contact');
    });

    $(window).scroll(function () {


        if (document.body.scrollTop > 0 || document.documentElement.scrollTop > scrollTopServices) {

            $('.DataEaseHeader').css({
                "background-color":"black",
                "opacity": "0.9"
            });

            $('.DataEaseImage').css({
                "background-size": "90%",
                "background-image": "url('img/logo.png')",
                "background-repeat": "no-repeat",
                "background-position": "left",
                "float": "left",
                "width":"50%",
                "z-index":"999",
                "min-width":"180px",
                "display": "block",
                "opacity": "",
                "position":"relative"
            }).html('');

            $('.HeaderText').css({
                "display": "none"
            });

        }

        if (document.body.scrollTop > (scrollTopServices - 30) || document.documentElement.scrollTop > scrollTopBenefits) {
//                    $('.HeaderText').html('Services');
            $('.DataEaseHeader').css("background-color", "black");
            $('.DataEaseImage').css({
                "float": "right",
                "width":"40%",
                "min-width":"0",
                "z-index":"999",
                "cursor":"pointer",
                "max-width": "115px",
                "padding": "7px 0",
                "display": "block",
                "font-weight": "300"
            });

            $('.HeaderText').css({
                "display": "block",
                "font-size": "22px",
                "text-align": "left",
                "padding": "15px 0px",
                "font-weight": "300",
                "width": "50%",
                "color":"white",
                "background": "none",
                "float": "left"
            }).html('Services');
        }

        if (document.body.scrollTop > (scrollTopBenefits - 30) || document.documentElement.scrollTop > scrollTopConsultation) {
//                    $('.HeaderText').html('Benefits');
            $('.DataEaseHeader').css("background-color", "black");
            $('.DataEaseImage').css({
                "float": "right",
                "width":"40%",
                "min-width":"0",
                "z-index":"999",
                "max-width": "115px",
                "cursor":"pointer",
                "padding": "7px 0",
                "display": "block",
                "font-weight": "300"
            });

            $('.HeaderText').css({
                "display": "block",
                "font-size": "22px",
                "text-align": "left",
                "padding": "15px 0px",
                "width":"42%",
                "min-width":"0",
                "font-weight": "300",
                "color":"white",
                "background": "none",
                "float": "left"
            }).html('Benefits');
        }

        if (document.body.scrollTop > (scrollTopConsultation - 30) || document.documentElement.scrollTop > scrollTopContact) {
//                    $('.HeaderText').html('Consultation');
            $('.DataEaseHeader').css("background-color", "black");
            $('.DataEaseImage').css({
                "float": "right",
                "width":"40%",
                "min-width":"0",
                "z-index":"999",
                "max-width": "115px",
                "cursor":"pointer",
                "padding": "7px 0",
                "display": "block",
                "font-weight": "300"
            });


            $('.HeaderText').css({
                "display": "block",
                "font-size": "22px",
                "text-align": "left",
                "padding": "15px 0px",
                "font-weight": "300",
                "width":"42%",
                "min-width":"0",
                "color":"white",
                "background": "none",
                "float": "left"
            }).html('Consultation');
        }

        if (document.body.scrollTop > clientHeight - 750) {
//                    $('.HeaderText').html('Contact');
            $('.DataEaseHeader').css("background-color", "black");
            $('.DataEaseImage').css({
                "float": "right",
                "width":"40%",
                "min-width":"0",
                "z-index":"999",
                "max-width": "115px",
                "cursor":"pointer",
                "padding": "7px 0",
                "display": "block",
                "font-weight": "300"
            });

            $('.HeaderText').css({
                "display": "block",
                "font-size": "22px",
                "text-align": "left",
                "padding": "15px 0px",
                "font-weight": "300",
                "width":"42%",
                "min-width":"0",
                "color":"white",
                "background": "none",
                "float": "left"
            }).html('Contact');
        }
    });
}
else {

    $('.Services').click(function () {
        $('html, body').animate({ scrollTop: $('.container3').offset().top }, 'slow', 'swing', function () {
        });
    });

    $('.Benefits').click(function () {
        $('html, body').animate({scrollTop: $('.BenefitsContainer').offset().top - 45}, 'slow', 'swing', function () {
        });
    });

    $('.Consultation').click(function () {
        $('html, body').animate({scrollTop: $('.ConsultationContainer').offset().top - 45 }, 'slow', 'swing', function () {
        });
    });

    $('.Contact').click(function () {
        $('html, body').animate({scrollTop: $('.container4').offset().top }, 'slow', 'swing', function () {
        });
    });

}

function submitInfo(){

    var name = $('.NameInput').val();
    var email = $('.EmailInput').val();
    var company = $('.CompanyInput').val();

    var data = new FormData();
    data.append("name",name);
    data.append("email_to",email);
    data.append("company_name",company);


    if(name == ''){
        swal("Error", "Please enter your name", "error");
        return false;
    }
    if(email == ''){
        swal("Error", "Please enter eamil address","error");
        return false;
    }

    if(company == ''){
        swal("Error", "Please enter your company name","error");
        return false;
    }

    var apiLink ="../src/model/send_mail.php";

    $.ajax({
        url: apiLink,
        type: 'POST',
        data: data,
        cache: false,
//        dataType: 'json',
        async:false,
        processData: false, // Don't process the files
        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
        success: function(data, req, res)
        {
            swal("Thank You", "We will get back to you soon", "success");
            console.log("Data is : "+ data);
        },
        error: function(jqXHR, textStatus, errorThrown)
        {
            // Handle errors here

            var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

            if (!filter.test(email)) {
                swal("Oops...", "Email is not correct", "error");
            }

//            swal("Error","Error submitting data", "error");
            console.log('ERRORS: ' + errorThrown);
        }
    });

    $('.InputBlock').val('');
}

$(document).ready(function () {

    $.smoothScroll();
});

$(document).ready(function() {
    var Bullets = $(".BulletsBlock");
    var BringBlock = $(".BringUpBlock");
    var MaintenanceBlockBottom = $(".MaintenanceBlock").offset().top + $(".MaintenanceBlock").height() - 400;
    var BringUpBlockBottom = $('.BringUpBlock').offset().top + $(".BringUpBlock").height() ;
    var CapabilitiesBlockBottom = $('.CapabilitiesBlock').offset().top + $('.CapabilitiesBlock').height();
    var pos = BringBlock.offset();

    $(window).scroll(function() {

        var windowpos = $(window).scrollTop();

        if (windowpos >= (pos.top - 200) && windowpos < MaintenanceBlockBottom) {
            $(".BulletsBlock").css("display","table");
            Bullets.addClass("stick");

            if(windowpos >= (pos.top - 200) && windowpos < BringUpBlockBottom){
                $(".BulletsBlock li").first().css("color","#d41f26");
                $(".BulletsBlock li:nth-child(2)").css("color","#CCCCCC");
                $(".BulletsBlock li").last().css("color","#CCCCCC");
            }

            if(windowpos >= ( $('.CapabilitiesBlock').offset().top - 200 ) &&  windowpos < CapabilitiesBlockBottom){
                $(".BulletsBlock li").first().css("color","#CCCCCC");
                $(".BulletsBlock li:nth-child(2)").css("color","#d41f26");
                $(".BulletsBlock li").last().css("color","#CCCCCC");
            }

            if(windowpos >= ( $(".MaintenanceBlock").offset().top - 200 ) && windowpos < MaintenanceBlockBottom ){
                $(".BulletsBlock li").first().css("color","#CCCCCC");
                $(".BulletsBlock li:nth-child(2)").css("color","#CCCCCC");
                $(".BulletsBlock li").last().css("color","#d41f26");
            }
        } else {
            Bullets.removeClass("stick");
        }
    });
});

$('.DataEaseImage').on('click',function(){
    $('html, body').animate({ scrollTop:0 }, 'slow', 'swing', function (){});
});